const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const settingSchema = new Schema({
  createdAt: { type: Date, default: Date.now },
  artifactoryServer: { type: String, required: true },
  artifactoryAPIKey: { type: String, required: true },
  artifactoryRepository: { type: String, required: true },
  backendHost: { type: String, required: true }
});


module.exports =  settingSchema;
