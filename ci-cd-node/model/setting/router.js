const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();

router.route('/config')
  .get((...args) => controller.getConfig(...args));

router.route('/:id')
  .put((...args) => controller.update(...args));

module.exports = router;
