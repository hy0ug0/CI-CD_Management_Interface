const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deploySchema = new Schema({
  deploymentDate: { type: Date, default: Date.now },
  environment: { type: String, required: true },
  frontVersion: { type: String },
  backVersion: { type: String },
  keepData: { type: Boolean },
  instableBack: { type: Boolean },
  instableFront: { type: Boolean },
  status: { type: String, enum: ['CREATED', 'DEPLOYED', 'FAILED'], default: 'CREATED' }
});

module.exports =  deploySchema;
