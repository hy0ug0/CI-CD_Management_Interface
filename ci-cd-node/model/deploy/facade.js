const exec = require('child_process').exec;

const Facade = require('../../lib/facade');
const deploySchema = require('./schema');

class DeployFacade extends Facade {
  async run(body) {
    let deploymentRecord;
    try {
      deploymentRecord = await this.create(body);
    } catch (error) {
      throw new Error('An error has occured while creating the new deployment record in the DB.');
    }
    const shPromise = new Promise((resolve, reject) => {
      let scriptParams = '';
      if (body.frontVersion) {
        scriptParams += `-f ${body.frontVersion} `;
      }
      if (body.backVersion) {
        scriptParams += `-b ${body.backVersion} `;
      }
      if (body.instableFront) {
        scriptParams += '--instable-front ';
      }
      if (body.instableBack) {
        scriptParams += '--instable-back ';
      }
      if (body.keepData) {
        scriptParams += '--keepdata ';
      }
      exec(`scripts/start.sh -e ${body.environment} ${scriptParams}`, (error, stdout, stderr) => {
        console.log(stderr, 'STDERR');
        console.log(stdout, 'STDOUT');
        if (error !== null) {
          console.log(error);
          reject(error);
        } else {
          resolve({ output: stdout });
        }
      });
    });
    try {
      await shPromise;
      await this.update({ _id: deploymentRecord }, { $set: { status: 'DEPLOYED' } });
      return await this.findById(deploymentRecord);
    } catch (error) {
      await this.update({ _id: deploymentRecord }, { $set: { status: 'FAILED' } });
      throw new Error(error);
    }
  }
}

module.exports = new DeployFacade('Deploy', deploySchema);
