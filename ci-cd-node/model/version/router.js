const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();

router.route('/:type')
  .get((...args) => controller.getVersions(...args));

module.exports = router;
