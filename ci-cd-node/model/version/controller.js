const versionFacade = require('./facade');

class VersionController {
  constructor(facade) {
    this.facade = facade;
  }

  async getVersions(req, res, next) {
    const type = req.params.type;
    let hasExpired = true;
    const cachedValue = await this.facade.findLastOne({ requestedAt: -1 }, { type });
    if (cachedValue) {
      hasExpired = this.facade.checkIfCacheHasExpired(cachedValue);
    }
    if (hasExpired) {
      this.facade.getVersions(type)
        .then((result) => {
          this.facade.cacheValue(result, cachedValue, type);
          return res.status(200).json(result);
        })
        .catch(err => res.status(500).json(err.message));
    } else {
      res.status(200).json({ versions: cachedValue.versions });
    }
  }
}

module.exports = new VersionController(versionFacade);
