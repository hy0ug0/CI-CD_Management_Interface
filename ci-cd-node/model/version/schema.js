const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const versionSchema = new Schema({
  type: { type: String },
  requestedAt: { type: Date },
  versions: [String]
});

module.exports =  versionSchema;
