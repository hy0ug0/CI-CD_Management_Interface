const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();

router.route('/say-hello-to-thomas')
  .post((...args) => controller.sayHelloToThomas(...args));
router.route('/warn-for-deployment')
  .post((...args) => controller.warnForDeployment(...args));

module.exports = router;
