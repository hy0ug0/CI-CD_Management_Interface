const settingFacade = require('../model/setting/facade');

class Settings {
  constructor(settingFacade, server, key, repository, host) {
    this.settingFacade = settingFacade;
    this.server = server;
    this.key = key;
    this.repository = repository;
    this.host = host;
  }

  async init() {
    const existingSettings = await this.settingFacade.findLastOne({ createdAt: -1 });
    if (!existingSettings || existingSettings.length === 0) {
      const newSettings = await this.settingFacade.create({
        artifactoryServer: this.server,
        artifactoryAPIKey: this.key,
        artifactoryRepository: this.repository,
        backendHost: this.host
      });
      return newSettings;
    }
    return existingSettings;
  }

  async getServer() {
    const document = await this.settingFacade.findLastOne({ createdAt: -1 }, { }, { artifactoryServer: 1 });
    return document.artifactoryServer;
  }

  async getKey() {
    const document = await this.settingFacade.findLastOne({ createdAt: -1 }, { }, { artifactoryAPIKey: 1 });
    return document.artifactoryAPIKey;
  }

  async getRepository() {
    const document = await this.settingFacade.findLastOne({ createdAt: -1 }, { }, { artifactoryRepository: 1 });
    return document.artifactoryRepository;
  }
}

module.exports = new Settings(
  settingFacade,
  process.env.ARTIFACTORY_SERVER,
  process.env.ARTIFACTORY_API_KEY,
  process.env.ARTIFACTORY_REPOSITORY,
  process.env.BACKEND_HOST
);
