const dockerFacade = require('./facade');

class DockerController {
  constructor(facade) {
    this.facade = facade;
  }

  getContainers(req, res, next) {
    this.facade.getContainers()
      .then((result) => {
        res.status(200).json(result);
      })
      .catch(err => res.status(500).json(err.message));
  }

  stopContainer(req, res, next) {
    this.facade.stopContainer(req.params.containerId)
      .then((result) => {
        res.status(200).json(result);
      })
      .catch(err => res.status(500).json(err.message));
  }

  startContainer(req, res, next) {
    this.facade.startContainer(req.params.containerId)
      .then((result) => {
        res.status(200).json(result);
      })
      .catch(err => res.status(500).json(err.message));
  }

  restartContainer(req, res, next) {
    this.facade.restartContainer(req.params.containerId)
      .then((result) => {
        res.status(200).json(result);
      })
      .catch(err => res.status(500).json(err.message));
  }
}

module.exports = new DockerController(dockerFacade);
