const Docker = require('dockerode');

class DockerFacade {
  constructor(docker) {
    this.docker = new docker({ socketPath: '/var/run/docker.sock' });
  }

  async getContainers() {
    const containersInfo = await [];
    const containers = await this.docker.listContainers({ all: true });
    containers.forEach((containerInfo) => {
      const utilsContainerInfo = {
        id: containerInfo.Id,
        name: containerInfo.Names[0],
        image: containerInfo.Image,
        port: containerInfo.Ports[0] ? containerInfo.Ports[0].PublicPort : null,
        state: containerInfo.State,
        status: containerInfo.Status
      };
      containersInfo.push(utilsContainerInfo);
    });

    const containerGroups = await {
      cicdContainers: { id: 'cicd', groupName: 'CI/CD Management', containers: [] },
      testingContainers: { id: 'testing', groupName: 'Testing', containers: [] },
      stagingContainers: { id: 'staging', groupName: 'Staging', containers: [] },
      stableContainers: { id: 'stable', groupName: 'Stable', containers: [] },
      otherContainers: { id: 'other', groupName: 'Other', containers: [] }
    };
    containersInfo.forEach((container) => {
      const containerName = container.name;
      if (containerName.includes('ci-cd')) {
        containerGroups.cicdContainers.containers.push(container);
      } else if (containerName.includes('testing')) {
        containerGroups.testingContainers.containers.push(container);
      } else if (containerName.includes('staging')) {
        containerGroups.stagingContainers.containers.push(container);
      } else if (containerName.includes('stable')) {
        containerGroups.stableContainers.containers.push(container);
      } else {
        containerGroups.otherContainers.containers.push(container);
      }
    });
    return containerGroups;
  }

  _createContainerEntity(containerId) {
    return this.docker.getContainer(containerId);
  }

  stopContainer(containerId) {
    return this._createContainerEntity(containerId).stop();
  }

  startContainer(containerId) {
    return this._createContainerEntity(containerId).start();
  }

  restartContainer(containerId) {
    return this._createContainerEntity(containerId).start();
  }
}

module.exports = new DockerFacade(Docker);
