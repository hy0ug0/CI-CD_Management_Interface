const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();

router.route('/')
  .get((...args) => controller.getContainers(...args));

router.route('/stop/:containerId')
  .get((...args) => controller.stopContainer(...args));
router.route('/start/:containerId')
  .get((...args) => controller.startContainer(...args));
router.route('/restart/:containerId')
  .get((...args) => controller.restartContainer(...args));

module.exports = router;
