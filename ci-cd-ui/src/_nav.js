export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-grid',
    },
    {
      name: 'Deployment',
      url: '/deployment',
      icon: 'icon-rocket',
    },
    {
      name: 'History',
      url: '/history',
      icon: 'icon-list',
    },
    {
      name: 'Docker',
      url: '/docker',
      icon: 'icon-speedometer',
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'icon-settings',
    },
  ],
};
