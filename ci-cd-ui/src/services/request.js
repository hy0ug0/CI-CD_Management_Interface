export default {
  handleError(error) {
    this.$log.error(error);
    this.error = `An error has occured: ${error.body}`;
    if (this.isLoading) {
      this.isLoading = false;
    }
    if (!this.showErrorAlert) {
      this.showErrorAlert = true;
    }
    this.$toasted.show(`An error has occured: ${error.body}`);
  },
};
